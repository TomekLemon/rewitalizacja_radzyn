$(document).ready(function () {
	// scrollowanie
	$(".upper-nav ul li a").click(function () {
		var elementClick = $(this).attr("href");
		var body = $('body, html');
		var destination = $(elementClick);
		var distance = $(destination).offset().top;
        
        $(body).animate({scrollTop: distance - 55}, 1100);
        
		return false;
	});
    
    $('a#poster, ul.news-gallery li a').colorbox({
        scalePhotos: true,
        maxWidth: 1000,
    });
});