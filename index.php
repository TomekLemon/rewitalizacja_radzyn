<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="assets/css/colorbox.css"/>
        <link rel="stylesheet" href="assets/css/default.css?ver=4"/>
        <link rel="icon" type="image/png" href="favicon.png"/>
        <title>Rewitalizacja Miasta Radzyń Podlaski</title>
    </head>
    <body>
        <div class="container">
            <header id="header" class="clearfix">
                <div class="brand pull-left clearfix">
                    <a href="">
                        <img src="assets/images/logo.png" class="logo" alt="Radzy podlaski" />
                        <h1><span>Miasto</span><br />
                        Radzyń Podlaski</h1>
                    </a>
                </div>
                <div class="upper-nav pull-right">
                    <ul class="unstyled inline">
                        <li>
                            <a href="#cele">Cele rewitalizacji</a>
                        </li>
                        <li>
                            <a href="#aktualnosci">Aktualności</a>
                        </li>
                        <li>
                            <a href="#kontakt">Kontakt</a>
                        </li>
                    </ul>
                </div>
            </header>
            <div id="content" class="clearfix">
                <div class="grey-bg"></div>
                <div class="cele clearfix" id="cele">
                    <div class="narrow-col section-title">
                        <h2>Cele rewitalizacji</h2>
                        <img src="assets/images/miasto.jpg" alt="Miasto Radazyń Podlaski z lotu ptaka" />
                        <!--Możliwe że będą pliki do pobrania-->
                    </div>
                    <div class="wide-col clearfix">
                        <div class="half-col">
                            <h3>Podstawowe definicje związane<br />z rewitalizacją</h3>
                            <p><strong>Rewitalizacja</strong> jest zbiorem kompleksowych działań, prowadzonych na rzecz lokalnej społeczności, przestrzeni, gospodarki i środowiska, realizowana na obszarze zdegradowanym, i wykorzystująca jego czasem niedostrzeżony potencjał.</p>
                            <p>Aby mówić o obszarze zdegradowanym należy rozpocząć od <strong>szczegółowej diagnozy</strong>, obejmującej teren całego miasta. W pierwszej kolejności ustalany jest obszar, gdzie koncentrują się <strong>negatywne zjawiska społeczne</strong> (w szczególności ubóstwo, przestępczość, bezrobocie, niski poziom edukacji lub kapitału społecznego). Kolejnym krokiem jest sprawdzenie, czy na wskazanym wyżej obszarze występują <strong>negatywne zjawiska  gospodarcze, środowiskowe, przestrzenno-funkcjonalno, techniczne</strong>. Jeżeli zdiagnozowane zostanie choć jedno z nich – mamy do czynienia z <strong>obszarem zdegradowanym</strong>. Obszar taki, lub jego część, na którym z uwagi na istotne znaczenie dla rozwoju lokalnego zamierza się prowadzić rewitalizację,  nazywamy <strong>obszarem rewitalizacji</strong>.</p>
                            <p>Dla obszaru rewitalizacji przygotowywany jest <strong>program rewitalizacji</strong>, który musi być  odpowiedzią na  zdiagnozowane problemy a przede wszystkim  służyć wyprowadzeniu obszaru zdegradowanego ze stanu kryzysowego. Rewitalizacja powinna być prowadzona przy udziale <strong>interesariuszy rewitalizacji</strong>, którymi są:  mieszkańcy obszaru rewitalizacji, właściciele, użytkownicy wieczyści nieruchomości i podmioty zarządzające nieruchomościami  znajdującymi się na tym obszarze, w tym spółdzielnie mieszkaniowe, wspólnoty mieszkaniowe i towarzystwa budownictwa społecznego. Ponad to: podmioty prowadzące lub zamierzające prowadzić działalność gospodarczą  lub społeczną na terenie gminy, w tym organizacje i pozarządowe i grupy nieformalne, jednostki samorządu terytorialnego i ich jednostki organizacyjne, organy władzy publicznej, podmioty realizujące na obszarze rewitalizacji uprawnienia Skarbu Państwa a także wszyscy pozostali  mieszkańcy gminy, którzy chcą uczestniczyć w tym procesie.</p>
                            <p>Rewitalizacja zakłada optymalne wykorzystanie specyficznych uwarunkowań danego obszaru oraz wzmacnianie jego lokalnych potencjałów (w tym także kulturowych) i jest procesem wieloletnim, prowadzonym przez interesariuszy  tego procesu, w tym przede wszystkim we współpracy z lokalną społecznością.
                            </p>
                            <h3>Dla prowadzenia rewitalizacji<br />wymagane są:</h3>
                            <p>Rewitalizacja jest zbiorem kompleksowych działań, prowadzonych na rzecz lokalnej społeczności, przestrzeni, gospodarki i środowiska, realizowana na obszarze zdegradowanym, i wykorzystująca jego czasem niedostrzeżony potencjał.</p>
                            <ul>
                                <li>względnienie rewitalizacji jako istotnego elementu całościowej wizji rozwoju Miasta,</li>
                                <li>pełna diagnoza służąca wyznaczeniu obszaru rewitalizacji oraz analizie dotykających go problemów (diagnoza obejmuje kwestie społeczne oraz gospodarcze lub przestrzenno-funkcjonalne lub techniczne lub środowiskowe),</li>
                                <li>ustalenie hierarchii potrzeb w zakresie działań rewitalizacyjnych,</li>
                                <li>właściwy dobór narzędzi i interwencji do potrzeb i uwarunkowań danego obszaru,</li>
                                <li>zsynchronizowanie działań w sferze społecznej, gospodarczej, przestrzenno-funkcjonalnej, technicznej, środowiskowej;</li>
                                <li>koordynacja prowadzonych działań oraz monitorowanie i ewaluacja skuteczności rewitalizacji,</li>
                                <li>realizacja zasady partnerstwa polegającej na włączeniu partnerów w procesy programowania i realizacji projektów rewitalizacyjnych w ramach programów operacyjnych oraz konsekwentnego, otwartego i trwałego dialogu z tymi podmiotami i grupami, których rezultaty rewitalizacji mają dotyczyć.</li>
                            </ul>
                        </div>
                        <div class="half-col">
                            <p><strong>Stan kryzysowy</strong> - stan spowodowany koncentracją negatywnych zjawisk społecznych (w szczególności bezrobocia, ubóstwa, przestępczości, niskiego poziomu edukacji lub kapitału społecznego, niewystarczającego poziomu uczestnictwa w życiu publicznym i kulturalnym), współwystępujących z negatywnymi zjawiskami w co najmniej jednej z następujących sfer:</p>
                            <ul>
                                <li>gospodarczej (w szczególności w zakresie niskiego stopnia przedsiębiorczości, słabej kondycji lokalnych przedsiębiorstw),</li>
                                <li>środowiskowej (w szczególności w zakresie przekroczenia standardów jakości środowiska, obecności odpadów stwarzających zagrożenie dla życia, zdrowia, ludzi bądź stanu środowiska),</li>
                                <li>przestrzenno-funkcjonalnej (w szczególności w zakresie niewystarczającego wyposażenia w infrastrukturę techniczną i społeczną, braku dostępu do podstawowych usług lub ich niskiej jakości, niedostosowania rozwiązań urbanistycznych do zmieniających się funkcji obszaru, niskiego poziomu obsługi komunikacyjnej, deficytu lub niskiej jakości terenów publicznych),</li>
                                <li>technicznej (w szczególności w zakresie degradacji stanu technicznego obiektów budowlanych, w tym o przeznaczeniu mieszkaniowym, oraz braku funkcjonowania rozwiązań technicznych umożliwiających efektywne korzystanie z obiektów budowlanych, w szczególności w zakresie energooszczędności i ochrony środowiska). Skalę negatywnych zjawisk odzwierciedlają mierniki rozwoju opisujące powyższe sfery, które wskazują na niski poziom rozwoju lub dokumentują silną dynamikę spadku poziomu rozwoju, w odniesieniu do wartości dla całej gminy.</li>
                            </ul>
                            <p><strong>Obszar zdegradowany</strong> - obszar, na którym zidentyfikowano stan kryzysowy. Dotyczy to najczęściej obszarów miejskich, ale także wiejskich. Obszar zdegradowany może być podzielony na podobszary, w tym podobszary nieposiadające ze sobą wspólnych granic pod warunkiem stwierdzenia sytuacji kryzysowej na każdym z podobszarów.</p>
                            <p><strong>Obszar zdegradowany</strong> - obszar obejmujący całość lub część obszaru zdegradowanego, cechującego się szczególną koncentracją negatywnych zjawisk, z uwagi na istotne znaczenie dla rozwoju lokalnego, zamierza się prowadzić rewitalizację.</p> 
                            <p><strong>Obszar rewitalizacji</strong> może być podzielony na podobszary, w tym podobszary nieposiadające ze sobą wspólnych granic, <strong>lecz nie może obejmować terenów większych niż 20% powierzchni gminy oraz zamieszkałych przez więcej niż 30% mieszkańców gminy</strong>. W skład obszaru rewitalizacji mogą wejść obszary występowania problemów przestrzennych, takich jak tereny poprzemysłowe (w tym poportowe i powydobywcze), powojskowe lub pokolejowe, wyłącznie w przypadku, gdy przewidziane dla nich działania są ściśle powiązane z celami rewitalizacji dla danego obszaru rewitalizacji.</p>
                            <p><strong>Obszar zdegradowany</strong> - inicjowany, opracowany i uchwalony przez radę gminy wieloletni program działań w sferze społecznej oraz gospodarczej lub przestrzenno-funkcjonalnej lub technicznej lub środowiskowej, zmierzający do wyprowadzenia obszarów rewitalizacji ze stanu kryzysowego oraz stworzenia warunków do ich zrównoważonego rozwoju, stanowiący narzędzie planowania, koordynowania i integrowania różnorodnych aktywności w ramach rewitalizacji.</p>
                        </div>
                    </div>
                </div>
                <div class="aktualnosci clearfix" id="aktualnosci">
                    <div class="wide-col">
                        <div class="news-item clearfix">
                            <div class="half-col">
                                <h2 class="larger-title">Zaproszenie na konferencję podsumowującą  opracowanie Miejskiego Programu Rewitalizacji Miasta Radzyń Podlaski na lata 2016-2023</h2>
                                
                                <p>
                                    W związku z zakończeniem prac nad przygotowaniem <strong>Miejskiego Programu Rewitalizacji Miasta Radzyń Podlaski na lata 2016-2023</strong>, w ramach realizacji projektu pn."Opracowanie Miejskiego Programu Rewitalizacji Miasta Radzyń Podlaski na lata 2016-2023", współfinansowanego ze środków Unii Europejskiej w ramach Programu Operacyjnego Pomoc Techniczna 2014-2020, mam przyjemność zaprosić Państwa na konferencję podsumowującą realizację projektu. </p>
                                <p>Konferencja odbędzie się <strong>17 maja 2017 r. o godzinie 13:00</strong> w sali konferencyjnej Urzędu Miasta Radzyń Podlaski, przy ul. Warszawskiej 32.</p>
                                <p>
                                    Rewitalizacja miast jest procesem wieloaspektowym, w którym coraz większą rolę odgrywają czynniki społeczne. Umiejętne dostosowanie planowanych przedsięwzięć do zidentyfikowanych potrzeb lokalnych oraz istniejących uwarunkowań jest szansą na osiągnięcie pożądanych efektów rewitalizacji.
                                </p>
                                <p>W Miejskim Programie Rewitalizacji Miasta Radzyń Podlaski na lata 2016-2023 ujętych zostało szereg projektów, których realizacja przyczyni się do ograniczenia zjawisk kryzysowych zidentyfikowanych w mieście oraz rozwiązywania problemów o charakterze społecznym, gospodarczym, środowiskowym, technicznym oraz przestrzenno-funkcjonalnym.</p>
                                <p>Chcąc przybliżyć Państwu proces opracowania Programu Rewitalizacji oraz przedstawić znajdujące się w nim projekty, zachęcam serdecznie do uczestnictwa w konferencji wszystkie osoby zainteresowane lub zaangażowane w rozwój społeczno-gospodarczy i przestrzenny Miasta Radzyń Podlaski: mieszkańców, przedstawicieli organizacji pozarządowych, przedstawicieli instytucji samorządowych i państwowych oraz przedsiębiorców.</p>
                                <p class="text-right">
                                    Burmistrz Miasta<br />
                                    Radzyń Podlaski<br />
                                    <strong>Jerzy Rębek</strong>
                                </p>
                            </div>
                            <div class="half-col">
                                <a href="assets/download/plakat-maj-2017.jpg" class="cboxElement">
                                    <p>kliknij aby zobaczyć plakat</p>
                                    <img src="assets/download/plakat-maj-2017.jpg?ver=2" alt="Plakat Miejski Program Rewitalizacji Miasta Radzyń Podlaski" />
                                </a>
                                <a href="assets/download/miejski-program.pdf" target="_blank">
                                    <p>Miejski Program Rewitalizacji Miasta Radzyń Podlaski</p>
                                    <img src="assets/download/miejski-program.jpg" alt="Miejski Program Rewitalizacji Miasta Radzyń Podlaski" />
                                </a>
                            </div>
                        </div>
                        <div class="news-item clearfix">
                            <div class="half-col">
                                <h2 class="larger-title">Informacja z pierwszych konsultacji społecznych</h2>
                                <p>
                                    W dniu 21 lutego 2017 r. w Sali Konferencyjnej Urzędu Miasta Radzyń Podlaski miało miejsce pierwsze spotkanie konsultacyjne w ramach cyklu spotkań przygotowujących do opracowania Miejskiego Programu Rewitalizacji dla Miasta Radzyń Podlaski n lata 2016-2023.</p>
                                    <p>Podczas spotkania została przedstawiona metodologia na postawie, której zdiagnozowano sytuacje kryzysowe występujące na terenie miasta w obszarach: społecznym, gospodarczym, środowiskowym i infrastrukturalnym. Przedstawiono również wyniki dokonanej analizy oraz wypływające z niej wnioski. Uczestnikom spotkania konsultacyjnego wskazano kluczowe warunki umożliwiające zakwalifikowanie poszczególnych części Miasta do tak zwanego obszaru objętego rewitalizacją oraz przedstawiono proponowany obszar Radzynia Podlaskiego, dla którego opracowany zostanie Program Rewitalizacji. Obszar ten obejmuje tereny od tzw. Koszar do ulicy Budowlanej włączając w to tereny zabytkowego parku i pałacu znajdującego się w centrum miasta. 
                                </p>
                                <p>
                                    Kolejne spotkania konsultacyjne odbędą się w dniach:
                                    24.02.2017 godz. 14:00 – Konsultacje dedykowane organizacjom pozarządowym
                                    24.02.2017 godz. 16:00 – Konsultacje dedykowane przedsiębiorcom 
                                    28.02.2017 godz. 11:00 – Otwarte konsultacje społeczne dla wszystkich zainteresowanych
                                    opracowywanym Miejskim Programem Rewitalizacji
                                </p>
                                <p>Serdecznie zapraszamy do czynnego uczestnictwa w konsultacjach</p>
                            </div>
                            <div class="half-col">
                                <a href="assets/download/prezentacja.pdf">
                                    <p>kliknij aby zobaczyć prezentację</p>
                                    <img src="assets/download/prezentacja.jpg" alt="Informacja z pierwszych konsultacji społecznych" />
                                </a>
                                <h2 class="text-center">Galeria zdjęć</h2>
                                <ul class="unstyled news-gallery">
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0001.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0001.jpg" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0002.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0002.jpg" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0003.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0003.jpg" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0004.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0004.jpg" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0005.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0005.jpg" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0006.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0006.jpg" alt="" />
                                        </a>
                                    </li>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0007.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0007.jpg" alt="" />
                                        </a>
                                    </li>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0008.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0008.jpg" alt="" />
                                        </a>
                                    </li>
                                    </li>
                                    <li>
                                        <a href="assets/download/konsultacje/konsultacje_0009.jpg" class="cboxElement">
                                            <img src="assets/download/konsultacje/konsultacje_0009.jpg" alt="" />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="news-item clearfix">
                            <div class="half-col">
                                <h2 class="larger-title">Porozmawiajmy o rewitalizacji Radzynia Podlaskiego</h2>
                                <p>
                                    W związku z prowadzonymi pracami nad opracowaniem Miejskiego Programu Rewitalizacji dla Miasta Radzyń Podlaski oraz zakończeniem etapu polegającego na wyznaczeniu obszarów „zdegradowanych” zapraszamy na Otwarte Konsultacje Społeczne dotyczące w/w dokumentu strategicznego. Państwa obecność oraz pomysły, na to jakie działania powinny być wdrożone w celu poprawy sytuacji społecznej na terenie Miasta, pozwolą na opracowanie i wdrożenie najtrafniejszych działań naprawczych.
                                </p>
                            </div>
                            <div class="half-col">
                                <a href="assets/download/plakat-luty-2017.jpg" class="cboxElement">
                                    <img src="assets/download/plakat-luty-2017.jpg?ver=2" alt="Porozmawjamy o rewitalizacji Radzynia Podlaskiego" />
                                </a>
                            </div>
                        </div>
                        <div class="news-item clearfix">
                            <div class="half-col">
                                <h2 class="larger-title">Miasto Radzyń Podlaski przystąpiło 
                                    do opracowania Miejskiego Programu 
                                    Rewitalizacji Miasta Radzyń Podlaski 
                                    na lata 2016-2023
                                </h2>
                                <p>Miejski Program Rewitalizacji Miasta Radzyń Podlaski będzie opracowany zgodnie z Wytycznymi w zakresie rewitalizacji w programach operacyjnych na lata 2014–2020 zatwierdzonymi w dniu 
                                    3 lipca 2015 r. przez Ministra Infrastruktury i Rozwoju.</p>
                                <p>Program ten będzie stanowił podstawę do podjęcia kompleksowych działań rewitalizacyjnych na zdegradowanym obszarze Miasta Radzyń Podlaski  wymagającym szczególnego wsparcia. Ponadto umożliwi efektywne pozyskiwanie dofinansowania projektów ze środków Unii Europejskiej w perspektywie finansowej na lata 2014–2020. Zaplanowane do realizacji projekty w ramach ww. dokumentu przyczynią się do pobudzenia aktywności społecznej i przedsiębiorczości mieszkańców, przywrócenia estetyki i ładu przestrzennego, ochrony środowiska naturalnego, zachowania dziedzictwa kulturowego, a tym samym poprawy jakości życia społeczności lokalnej.</p>
                                <p>Rewitalizacja powinna być kompleksowym i efektywnym procesem, mającym za zadanie wyprowadzenie obszarów zdegradowanych ze stanu kryzysowego, spowodowanego koncentracją negatywnych zjawisk (społecznych, gospodarczych, środowiskowych, przestrzenno-funkcjonalnych i technicznych) przy aktywnym udziale i współpracy z lokalną społecznością.</p>
                                <p>Pierwszym etapem prac nad Programem Rewitalizacji jest całościowa <strong>diagnoza Miasta Radzyń Podlaski</strong> umożliwiająca wyznaczenie obszaru zdegradowanego oraz obszaru rewitalizacji, charakteryzujących się największą kumulacją problemów i lokalnych potencjałów w ww. sferach.</p>
                                <p><strong>Zachęcamy wszystkich mieszkańców, organizacje pozarządowe, przedsiębiorców i wszystkie inne osoby oraz podmioty, które chcą mieć wpływ na rozwój Miasta do włączenia się do prac nad diagnozą.</strong> </p>
                                <h2>Czym jest rewitalizacja?</h2>
                                <p>Rewitalizacja to zestaw działań mających na celu poprawę sytuacji w mieście, w obszarach, na których zidentyfikowano stan kryzysowy (obszary problemowe). Działania te obejmują <strong>sferę społeczną, gospodarczą, środowiskową, techniczną oraz przestrzenną</strong>. Dzięki ich realizacji ma nastąpić poprawa warunków do życia, wypoczynku i rozwoju gospodarczego.</p>
                            </div>
                            <div class="half-col">
                                <h2>Czym jest program rewitalizacji??</h2>
                                <p>Program Rewitalizacji jest <strong>planem wyprowadzenia obszarów zdegradowanych ze stanu kryzysowego</strong>. Działania zawarte w tym Programie będą służyły rozwiązaniu najistotniejszych problemów społecznych danego obszaru oraz towarzyszących im problemów w sferze gospodarczej, przestrzenno-funkcjonalnej, technicznej oraz środowiskowej. Prowadzone działania będą miały charakter systemowy i posłużą rozwojowi społecznemu, gospodarczemu oraz przestrzennemu. Będą mogły być finansowane ze środków publicznych (budżetu gminy, kraju, środków europejskich) oraz prywatnych. </p>
                                <p><strong>Udział społeczności</strong> lokalnej jest <strong>kluczowy</strong> dla opracowania Programu Rewitalizacji oraz diagnozy społeczno-gospodarczej, w tym wyznaczenia obszarów rewitalizacji. Działania planowane w ramach Programu mają służyć poprawie warunków życia i zwiększaniu aktywności gospodarczej mieszkańców. Dlatego też <strong>zachęcamy do włączenia się do prac nad dokumentem</strong>.</p>
                                <p>W związku z tym, zwracamy się do Państwa z prośbą o aktywne wzięcie udziału w procesie diagnostycznym gminy i wypełnienie poniżej zamieszczonej ankiety.</p>
                                <a href="assets/download/ankieta.pdf" download>Przejdź do ankiety</a>
                                <p>nkietę będzie można wypełnić <strong>do dnia 09.12.2016 r.</strong>.</p>
                                <p>Jednocześnie zapraszamy Państwa na konferencję pt. „Rola społeczności lokalnej w procesie rewitalizacji” inaugurującej opracowanie Miejskiego Programu Rewitalizacji Miasta Radzyń Podlaski na lata 2016-2023, która odbędzie się <strong>05 grudnia 2016 r. o godzinie 11:00 w Radzyńskim Ośrodku Kultury</strong> przy ul. Jana Pawła II 4, 23-300 Radzyń Podlaski (sala kinowa)</p>
                                <a href="assets/download/plakat.png" id="poster">Zobacz plakat</a>
                                <p>oraz warsztaty dla przedstawicieli sektorów publicznego, społecznego i gospodarczego, które umożliwią poznanie oczekiwań i potrzeb interesariuszy rewitalizacji oraz dokładną diagnozę gminy w celu wyznaczenia obszaru zdegradowanego i obszaru rewitalizacji. <strong>Szczegółowe informacje dotyczące ww. spotkań konsultacyjnych zostaną podane w późniejszym terminie.</strong></p>
                                <p>Jesteśmy przekonani, że jako aktywni mieszkańcy, przedsiębiorcy i przedstawiciele organizacji pozarządowych oraz instytucji publicznych, wniesiecie Państwo wiele uwag i propozycji, które przyczynią się do zdiagnozowania kluczowych problemów gminy koniecznych do rozwiązania w procesie rewitalizacji, a także lokalnych potencjałów.</p>
                                <p class="text-right">
                                    Burmistrz Miasta<br />
                                    <strong>Jerzy Rębek</strong>
                                </p>
                            </div>
                        </div>
                        <div class="zaproszenie">
                            <div class="half-col">
                                <h2 class="larger-title">Zaproszenie na konferencję dotyczącą opracowania Miejskiego Programu Rewitalizacji Miasta Radzyń Podlaski na lata 2016-2023</h2>
                                <p>Burmistrz Miasta Radzyń Podlaski zaprasza interesariuszy rewitalizacji Miasta Radzyń Podlaski na konferencję rozpoczynającą proces związany z opracowaniem Miejskiego Programu Rewitalizacji Miasta Radzyń Podlaski na lata 2016-2023, która odbędzie się w dniu 05 grudnia 2016 r. o godzinie 10:00 w Radzyńskim Ośrodku Kultury przy ul. Jana Pawła II 4, 23-300 Radzyń Podlaski.</p>
                                <p>Podczas konferencji przedstawione zostaną zagadnienia dotyczące procesu rewitalizacji, jej uwarunkowań formalno-prawnych oraz źródeł finansowania projektów rewitalizacyjnych.</p>
                            </div>
                            <div class="half-col">
                                <p>Aktywny udział w konferencji mieszkańców, przedsiębiorców i przedstawicieli organizacji pozarządowych oraz instytucji publicznych pozwoli w pełni zdiagnozować kluczowe problemy konieczne do rozwiązania w procesie rewitalizacji, a także wykorzystać lokalne potencjały Miasta Radzyń Podlaski.</p>
                                <p class="text-right">
                                    Burmistrz Miasta<br />
                                    <strong>Jerzy Rębek</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="narrow-col section-title">
                        <h2>Aktualności</h2>
                        <img src="assets/images/aktualnosci.jpg" alt="Miasto Radazyń Podlaski z lotu ptaka" />
                    </div>
                </div>
                <div class="kontakt clearfix" id="kontakt">
                    <div class="narrow-col section-title">
                        <h2>Kontakt</h2>
                    </div>
                    <div class="wide-col clearfix">
                        <div class="half-col">
                            <h2>Urząd Miasta Radzyń Podlaski</h2>
                            <p>ul. Warszawska 32<br />
                                21-300 Radzyń Podlaski</p>
                            <p>tel. sekretariat +48 <strong>83 351-24-60<br /></strong>
                                fax: +48 83 352-80-85</p>
                            <p>e-mail: sekretariat@radzyn-podl.pl<br />
                                www.radzyn-podl.pl</p>
                        </div>
                        <div class="half-col">
                            <p>Wykonawca programu rewitalizacji:<br />
                            Lubelska Fundacja Inicjatyw Ekologicznych</p>
                            <p>Arkadiusz Pisarski<br />
                            tel. +48 661 055 199</p>
                            <p>e-mail: arkadiusz.pisarski@lfie.pl</p>
                        </div>
                    </div>
                </div>
            </div>
            <img src="assets/images/ue.jpg" alt="Fundusze europejskie" class="ue-logos" />
        </div>
        <script src="assets/js/jquery-1.12.3.min.js"></script>
        <script src="assets/js/jquery.colorbox.js"></script>
        <script src="assets/js/scripts.js?ver=2"></script>
    </body>
</html>
